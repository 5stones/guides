# /etc/profile.d/ps1-colors.sh
# Sets up a standard colors for servers
#
# comment out the following in /etc/skel/.bashrc and every user's .bashrc:
#     if [ "$color_prompt" = yes ]; then
#         PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
#     else
#         PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
#     fi

production_env=yes
#use_git_prompt=yes


if [ "$production_env" = "yes" ]; then
    host_color='\[\e[01;31m\]'
else
    host_color='\[\e[01;33m\]'
fi

if [ "`id -u`" != "1000" ]; then
    user_color='\[\e[01;32m\]'
else
    user_color='\[\e[01;33m\]'
fi

if [ -n "$use_git_prompt" ]; then
    GIT_PS1_SHOWUPSTREAM="auto"
    GIT_PS1_SHOWDIRTYSTATE=true
    GIT_PS1_SHOWUNTRACKEDFILES=true

    PS1='${debian_chroot:+($debian_chroot)}'"$user_color"'\u\[\e[0m\]@'"$host_color"'\h\[\e[00m\]:\[\e[0;94m\]\w\[\e[0;35m\]$(__git_ps1 " [%s]") \[\e[36;1m\]\$ \[\e[0m\]'
else
    PS1='${debian_chroot:+($debian_chroot)}'"$user_color"'\u\[\e[0m\]@'"$host_color"'\h\[\e[00m\]:\[\e[0;94m\]\w \[\e[36;1m\]\$ \[\e[0m\]'
fi

unset production_env use_git_prompt user_color host_color

force_color_prompt=yes
