#!/bin/bash
# Updates the memory and swap usage in Cloudwatch for this instance
# Works with newer versions of `free` that no longer uses the "-/+ buffers/cache" line
# Requires packages: `sudo apt install awscli bc`

MEMORY_NAMESPACE="System/Linux"
INSTANCE_ID=`wget -q -O - http://169.254.169.254/latest/meta-data/instance-id`

# ram in mb

MEM_TOTAL=`free -m | awk '/Mem/ {print $2}'`
MEM_USED=`free -m | awk '/Mem/ {print $3}'`
MEM_LOAD=`echo "100.0 * $MEM_USED / $MEM_TOTAL" | bc -l`

aws cloudwatch put-metric-data --metric-name "MemoryUtilization" --unit Percent --value $MEM_LOAD --dimensions InstanceId=$INSTANCE_ID --namespace $MEMORY_NAMESPACE


# swap in mb

SWAP_TOTAL=`free -m | awk '/Swap/ {print $2}'`
if [ "$SWAP_TOTAL" -ne "0" ]; then
	SWAP_USED=`free -m | awk '/Swap/ {print $3}'`
	SWAP_LOAD=`echo "100.0 * $SWAP_USED / $SWAP_TOTAL" | bc -l`

	aws cloudwatch put-metric-data --metric-name "SwapUtilization" --unit Percent --value $SWAP_LOAD --dimensions InstanceId=$INSTANCE_ID --namespace $MEMORY_NAMESPACE
fi
