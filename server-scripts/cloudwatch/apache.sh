#!/bin/bash
# Updates Cloudwatch with Apache's worker % usage:
#   BusyWorkers / MaxWorkers
# requires packages: `sudo apt install awscli bc`

APACHE_NAMESPACE="Apache"
HOST="localhost"


INSTANCE_ID=`wget -q -O - http://169.254.169.254/latest/meta-data/instance-id`

# track apache workers
WORKERS_BUSY=`wget -q -O - http://$HOST/server-status?auto | awk '/BusyWorkers/ { print $2 }'`

# busy count
#aws cloudwatch put-metric-data --metric-name "WorkersUsed" --unit Count --value $WORKERS_BUSY --dimensions InstanceId=$INSTANCE_ID --namespace $APACHE_NAMESPACE

# idle count
#WORKERS_IDLE=`wget -q -O - http://$HOST/server-status?auto | awk '/IdleWorkers/ { print $2 }'`
#aws cloudwatch put-metric-data --metric-name "WorkersIdle" --unit Count --value $WORKERS_IDLE --dimensions InstanceId=$INSTANCE_ID --namespace $APACHE_NAMESPACE

# calculate maximum number of workers using the Scoreboard
WORKERS_MAX=`wget -q -O - http://$HOST/server-status?auto | awk '/Scoreboard/ { printf $2 }' | wc -c`

# busy %
if [ ! -z "$WORKERS_MAX" ]; then
	WORKERS_LOAD=`echo "100.0 * $WORKERS_BUSY / $WORKERS_MAX" | bc -l`

	aws cloudwatch put-metric-data --metric-name "WorkerUtilization" --unit Percent --value $WORKERS_LOAD --dimensions InstanceId=$INSTANCE_ID --namespace $APACHE_NAMESPACE
fi
