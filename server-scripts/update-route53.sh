#!/bin/bash
# Updates the A record of a domain name, which has a public IP but is not using an elastic IP
# author: Ken Michalak
#
# The IAM role or user needs the following IAM permission for the zone:
#     "Resource": [
#         "arn:aws:route53:::hostedzone/{the ZONE_ID below}"
#     ],
#     "Action": [
#         "route53:ChangeResourceRecordSets",
#         "route53:GetHostedZone",
#         "route53:ListResourceRecordSets"
#     ]
#
# To keep a name updated, add the following to cron:
#     @reboot /usr/local/bin/update-route53.sh {hostname}

# Configure the following
ZONE_ID=""
DOMAIN_NAME=""
TTL=300


# check argument
if [ -z "$1" ]; then
	host=`hostname`
	echo "Usage: $0 NAME"
	echo
	echo "ex: '$0 $host' to update the A record of '$host.$DOMAIN_NAME'."
	exit 1
fi

hostname="$1.$DOMAIN_NAME"

# exit if there's an error anywhere
set -e

# get our public ip from AWS
public_ip=`ec2metadata | awk '/public-ipv4/ {print $2}'`

# lookup the current IP from AWS
current=`aws route53 list-resource-record-sets --hosted-zone-id=$ZONE_ID --query "ResourceRecordSets[?Name == '$hostname.']" | awk -F'"' '/Value/ { print $4; exit; }'`

# lookup what the ip of the hostname is currently
#host_output=`host -4 "$hostname"`
#current=`echo "$host_output" | awk '/has address/ { print $4; exit; }'`


if [ "$current" != "$public_ip" ]; then
	# update the IP in route 53
	echo "Updating '$hostname' from '$current' to '$public_ip'"
	aws route53 change-resource-record-sets --hosted-zone-id=$ZONE_ID --change-batch='{
        "Comment": "'"Auto updating @ `date -Iseconds`"'",
        "Changes": [
            {
                "Action": "UPSERT",
                "ResourceRecordSet": {
                    "Name": "'"$hostname"'.",
                    "Type": "A",
                    "TTL": '"$TTL"',
                    "ResourceRecords": [
                        {
                            "Value": "'"$public_ip"'"
                        }
                    ]
                }
            }
        ]
}'
fi
