# Style Guides

Please read through and adhere to the appropriate standards for your project set forth here:
* [Javascript](https://github.com/airbnb/javascript)
* [Ruby](https://github.com/bbatsov/ruby-style-guide)
    * [Ruby on Rails](https://github.com/bbatsov/rails-style-guide)
    * Documentation: [RDoc](http://rdoc.rubyforge.org/RDoc/Markup.html) and [YARD](http://www.rubydoc.info/gems/yard/file/docs/GettingStarted.md)
* [HTML & CSS](#html-css)
    * [Style Guide](https://github.com/airbnb/css#sass)
    * [Organization](#html-css)


## HTML & CSS
Please read through and adhere to the standards set forth [here](https://github.com/airbnb/css#sass).

##### Web assets organization:

You can see a good working example of this here for reference: https://github.com/twbs/bootstrap

```
│  [sass|less]   
│   ├── mixins   
│   │   ├── foo.[scss|less]   
│   │   ├── bar.[scss|less]   
│   │   ├── ...   
│   │   ├── mixins.[scss|less]   
│   ├── variables.[scss|less]   
│   ├── _foo.[scss|less]    
│   ├── _bar.[scss|less]   
│   ├── ...   
│   ├── foobar.[scss|less]   

│  dist   
│   ├── css   
│   │   ├── boostrap.min.css   
│   │   ├── foobar.min.css   
│   ├── js   
```


##### mixins & mixins.[scss|less]

All mixins should be organized in discrete and modular files with names representing their contents.

mixins.[scss|less] should EXCLUSIVELY import all the available mixins so that in other files simply importing mixins.[scss|less] imports all mixins.



##### less & foobar.[scss|less]

The rest of the less files should also be organized in discrete and logical ways i.e. _header.[scss|less] should contain all the css for the header.

foobar.[scss|less] would EXCLUSIVELY import all the less partials pertaining to it. Typically the file would begin with the following:

```scss
// foobar.[scss|less]
@import 'variables.[scss|less]';
@import 'mixins/mixins.[scss|less]';

@import '_foo.[scss|less]';
...
```

This would make all the variables and mixins available to all partials.



##### dist

Ideally dist should be the destination of all the compiled &, in the case of a production environment, minified css and javascript files. In most cases, this file should likely be included in the `.gitignore` file and be compiled on deploy. If the application framework doesn't have a native way of compiling assets setup a `gulp` task to do so.

## Frameworks

All front-end development utilizes the bootstrap responsive framework. For documentation, see http://getbootstrap.com/
